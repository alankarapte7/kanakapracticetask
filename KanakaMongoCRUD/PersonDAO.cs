﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using MongoDB.Bson;

namespace KanakaMongoCRUD
{
    class PersonDAO
    {
        private readonly IMongoClient client;
        private readonly IMongoDatabase database;
        private readonly IMongoCollection<Person> collection;

        public PersonDAO()
        {
            this.client = new MongoClient("mongodb://localhost:27017");
            this.database = client.GetDatabase("personDB");
            this.collection = database.GetCollection<Person>("person");

        }

        /* 
         * reading data on various condition 
         */
        public void getAllPersons()
        {
            //  var personList = collection.Find(Builders<Person>.Filter.Empty).ToList<Person>();
            var personList = collection.AsQueryable<Person>().ToList();
            foreach (var person in personList)
            {
                Console.WriteLine("Id: " + person.Id);
                Console.WriteLine("Full Name: " + person.FirstName + " " + person.LastName);
                Console.WriteLine("Address: " + person.Address["city"] + ", " + person.Address["pincode"]);
                Console.WriteLine("-------------------------------------------");
            }
            Console.WriteLine("#############################################");
        }

        public void getPersonById(string id)
        {
            var personId = new ObjectId(id); //can use ObjectId.Parse(string) method too

            //var personList = collection.Find(Builders<Person>.Filter.Empty).ToList<Person>();
            var person = collection.AsQueryable<Person>().SingleOrDefault(p => p.Id == personId);

            Console.WriteLine("Id: " + person.Id);
            Console.WriteLine("Full Name: " + person.FirstName + " " + person.LastName);
            Console.WriteLine("Address: " + person.Address["city"] + ", " + person.Address["pincode"]);

            Console.WriteLine("#############################################");
        }

        public void getPersonsByLastName(string lastName)
        {
            //var personList = collection.Find(Builders<Person>.Filter.Empty).ToList<Person>();
            var personList = collection.AsQueryable<Person>().Where( p => p.LastName == lastName);
            foreach (var person in personList)
            {
                Console.WriteLine("Id: " + person.Id);
                Console.WriteLine("Full Name: " + person.FirstName + " " + person.LastName);
                Console.WriteLine("Address: " + person.Address["city"] + ", " + person.Address["pincode"]);
                Console.WriteLine("-------------------------------------------");
            }
            Console.WriteLine("#############################################");
        }

        public void searchPersonFirstName(string searchWord)
        {
            //var personList = collection.Find(Builders<Person>.Filter.Empty).ToList<Person>();
            var personList = collection.AsQueryable<Person>().Where(p => p.FirstName.Contains(searchWord) ).ToList();
            foreach (var person in personList)
            {
                Console.WriteLine("Id: " + person.Id);
                Console.WriteLine("Full Name: " + person.FirstName + " " + person.LastName);
                Console.WriteLine("Address: " + person.Address["city"] + ", " + person.Address["pincode"]);
                Console.WriteLine("-------------------------------------------");
            }
            Console.WriteLine("#############################################");
        }


        /* 
        * Creating data 
        */

        public void AddPerson()
        {
            Person person = new Person
            {
                FirstName = "Alankar",
                LastName = "Apte",
                Address = new BsonDocument { { "city", "Kolhapur" }, {"pincode","456546" } }
            };

            collection.InsertOne(person);

            Console.WriteLine("Documents inserted");
        }


        /* 
       * Updating data 
       */
        public void UpdatePerson()
        {
            /*
            var result = collection.UpdateOne(
                    Builders<Person>.Filter.Eq("_id", new ObjectId("5ecbffbb8da7990b6071800c")),
                    Builders<Person>.Update.Set("fname", "abhinav")
                                           .Set("lname", "Lake")
            );  
            */

            var result = collection.UpdateOne( p => p.Id == new ObjectId("5ecbffbb8da7990b6071800c") ,
                    Builders<Person>.Update.Set( p => p.FirstName, "Seema")
                                           .Set( p => p.LastName, "Sawal")
            ); //can use ObjectId.Parse(string) method, instead 'new Object(string)'


            Console.WriteLine("Documents Updated: " + result.ModifiedCount );
        }

        /* 
      * Deleteing data 
      */
        public void DeletePerson()
        {
           // var result = collection.DeleteOne( Builders<Person>.Filter.Eq("_id", ObjectId.Parse("5ecc0026091dd323505aa7d3")) );
           
            var result = collection.DeleteOne( p => p.Id == ObjectId.Parse("5ecc0026091dd323505aa7d3") );

            Console.WriteLine("Documents Updated: " + result.DeletedCount);
        }



    }
}
