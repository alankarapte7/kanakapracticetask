﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace KanakaMongoCRUD
{
    class Person
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement(elementName: "fname")]
        public string FirstName { get; set; }

        [BsonElement(elementName: "lname")]
        public string LastName { get; set; }

        [BsonElement(elementName: "address")]
        public BsonDocument Address { get; set; }  // we can create 'Address' class & can use Address class property here also
    }
}
