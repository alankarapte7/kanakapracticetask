﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;

namespace KanakaMongoCRUD
{
    class Program
    {
        static void Main(string[] args)
        {
            PersonDAO crudOperation = new PersonDAO();

            /* 
            * reading data on various condition 
            */

            //crudOperation.getAllPersons();
            //crudOperation.getPersonById("5ecbffbb8da7990b6071800c");
            //crudOperation.getPersonsByLastName("pare");
            //crudOperation.searchPersonFirstName("sha");

            /* 
            * creating data 
            */

            //crudOperation.AddPerson();

            /* 
            * Updating data 
            */

            //crudOperation.UpdatePerson(); 

            /* 
            * Updating data 
            */

            //crudOperation.DeletePerson();



            //by using BsonDocument as
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("personDB");
            var collection = database.GetCollection<BsonDocument>("person");

            collection.Find(new BsonDocument()).ForEachAsync( p => Console.WriteLine(p) );

            Console.ReadLine();
        }
    }
}
