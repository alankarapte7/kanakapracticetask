﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace KanakaMongoWithJsGridTask.Domain
{
    public class Address
    {

        [BsonElement(elementName: "city")]
        public string City { get; set; }

        [BsonElement(elementName: "pincode")]
        public int? Pincode { get; set; }
    }
}
