﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaMongoWithJsGridTask.Domain
{

    public class Person
    {        
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement(elementName:"fname")]
        public string Fname { get; set; }

        [BsonElement(elementName: "lname")]
        public string Lname { get; set; }

        [BsonElement(elementName: "address")]
        public Address Address { get; set; }

    }
}
