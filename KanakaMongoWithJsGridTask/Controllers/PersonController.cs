﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaMongoWithJsGridTask.Domain;
using KanakaMongoWithJsGridTask.Models;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;

namespace KanakaMongoWithJsGridTask.Controllers
{
    public class PersonController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult AddPerson()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddPerson(Person person)
        {
            if (ModelState.IsValid)
            {
                BsonDocument bsonPersonElement = new BsonDocument()
                {
                    { "fname", person.Fname},
                    { "lname", person.Lname},
                    { "address", new BsonDocument(){
                        { "city", person.Address.City},
                        { "pincode", person.Address.Pincode }
                    } },
                };

                var client = new MongoClient("mongodb://localhost:27017");
                var database = client.GetDatabase("personDB");
                var collection = database.GetCollection<BsonDocument>("person");
                await collection.InsertOneAsync(bsonPersonElement);

                return RedirectToAction("Index");
            }
            return View(person);
        }


        [HttpPost] [HttpGet]
        public JsonResult GetAllPersonsWithUsingPersonEntityList()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("personDB");
            var collection = database.GetCollection<Person>("person");
                               
            return Json(collection.Find(Builders<Person>.Filter.Empty).ToList<Person>());
        }

        [HttpPost] [HttpGet]
        public JsonResult GetAllPersonsUsingBsonDocumentList()
        {
            var client = new MongoClient("mongodb://localhost:27017");
            var database = client.GetDatabase("personDB");
            var collection = database.GetCollection<BsonDocument>("person");

            IList<BsonDocument> list = new List<BsonDocument>();

            foreach (var item in collection.Find(Builders<BsonDocument>.Filter.Empty).ToList())
            {
                list.Add(
                    new BsonDocument
                    {
                        { "fname", item["fname"]},
                        { "lname", item["lname"]},
                        { "address", new BsonDocument{
                            { "city",item["address"]["city"] },
                            { "pincode", item["address"]["pincode"] }
                        }}
                    });
            }
            return Json(Newtonsoft.Json.JsonConvert.DeserializeObject( list.ToJson()));
        }

    } // class

} //namespace
