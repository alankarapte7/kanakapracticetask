﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaMVCDemoTask.Domain;
using KanakaMVCDemoTask.Models;

namespace KanakaMVCDemoTask.Repository
{
    public class StudentRepository : IStudentRepository
    {
        private readonly AppDbContext appDbContext;
        public StudentRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public Student GetStudent(long id)
        {
            return appDbContext.Student.Find(id);
        }

        Student IStudentRepository.Delete(long id)
        {
            Student student = appDbContext.Student.Find(id);
            if (student!=null)
            {
                appDbContext.Student.Remove(student);
                appDbContext.SaveChanges();
            }
            return student;
        }

        IEnumerable<Student> IStudentRepository.GetAllStudents()
        {
            return appDbContext.Student;
        }

        Student IStudentRepository.Save(Student student)
        {
            appDbContext.Student.Add(student);
            appDbContext.SaveChanges();
            return student;
        }

        Student IStudentRepository.Update(Student studentChanges)
        {
            var entry = appDbContext.Student.Attach(studentChanges);

            entry.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            appDbContext.SaveChanges();
            return studentChanges;
        }
    }
}
