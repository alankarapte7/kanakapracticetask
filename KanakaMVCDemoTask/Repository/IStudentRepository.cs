﻿using KanakaMVCDemoTask.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaMVCDemoTask.Repository
{
  public  interface IStudentRepository
    {
        Student GetStudent(long id);
        IEnumerable<Student> GetAllStudents();
        Student Save(Student student);
        Student Update(Student studentChanges);
        Student Delete(long id);
    }
}
