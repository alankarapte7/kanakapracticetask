﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaMVCDemoTask.Domain;
using KanakaMVCDemoTask.Repository;
using KanakaMVCDemoTask.VO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KanakaMVCDemoTask.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentRepository studentRepository;
        private readonly ILogger<HomeController> logger;
        public StudentController(IStudentRepository studentRepository, ILogger<HomeController> logger)
        {
            this.studentRepository = studentRepository;
            this.logger = logger;
            logger.LogDebug(1, "NLog injected into StudentController");
        }

        [HttpGet]
        public IActionResult Index()
        {
            logger.LogInformation("index started");
            IEnumerable<Student> studentList = studentRepository.GetAllStudents();
            List<StudentVO> studentVOList = new  List<StudentVO>();

            foreach (Student student in studentList)
            {
                StudentVO studentVO = new StudentVO() {
                    Id = student.Id,
                    RollNo = student.RollNo,
                    Name = student.Name,
                    Contact = student.Contact
                };

                studentVOList.Add(studentVO);
            }

            logger.LogInformation("index end");
            return View(studentVOList);
        }

        [HttpGet]
        public IActionResult AddStudent()
                    {
            logger.LogInformation("AddStudent Get req.");
            return View();
        }

        [HttpPost]
        public IActionResult AddStudent(AddStudentVO addStudentVO)
        {
            logger.LogInformation("Add Student started", addStudentVO);
            if (ModelState.IsValid)
            {
                Student student = new Student()
                {
                    RollNo = addStudentVO.RollNo,
                    Name = addStudentVO.Name,
                    Contact = addStudentVO.Contact
                };

                studentRepository.Save(student);

                logger.LogInformation("AssStudent end");
                return RedirectToAction("Index");
            }

            logger.LogInformation("AssStudent end");
            return View(addStudentVO);
        }

        [HttpGet]
        public IActionResult UpdateStudent(long id)
        {
            logger.LogInformation("UpdateStudent started get req.", id);

            Student student = studentRepository.GetStudent(id);
            if (student != null)
            {
                StudentVO studentVO = new StudentVO()
                {
                    Id = student.Id,
                    RollNo = student.RollNo,
                    Name = student.Name,
                    Contact = student.Contact
                };
                logger.LogInformation("UpdateStudent ended");


                return View(studentVO);
            }
            logger.LogInformation("UpdateStudent ended");

            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult UpdateStudent(StudentVO studentVO)
        {
            logger.LogInformation("UpdateStudent started post req.", studentVO);

            if (ModelState.IsValid)
            {
                Student student = new Student()
                {
                    Id = studentVO.Id,
                    RollNo = studentVO.RollNo,
                    Name = studentVO.Name,
                    Contact = studentVO.Contact
                };

                studentRepository.Update(student);

                logger.LogInformation("UpdateStudent ended post req.");

                return RedirectToAction("Index");
            }
            logger.LogInformation("UpdateStudent ended post req.");

            return View(studentVO);
        }

        [HttpGet]
        public IActionResult DeleteStudent(long id)
        {
            logger.LogInformation("DeleteStudent started get req.",id);

            var s =  studentRepository.Delete(id);
            logger.LogInformation("DeleteStudent ended get req.");

            return RedirectToAction("Index");
        }
    }
}