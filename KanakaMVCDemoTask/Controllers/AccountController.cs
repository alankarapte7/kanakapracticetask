﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaMVCDemoTask.VO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace KanakaMVCDemoTask.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;


        public AccountController(SignInManager<IdentityUser> signInManager,
                                    RoleManager<IdentityRole> roleManager,
                                    UserManager<IdentityUser> userManager)
        {
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterVO registerVO)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser() {
                    Email=registerVO.Email,
                    UserName=registerVO.Email
                };

                var result = await userManager.CreateAsync(user, registerVO.Password);

                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent:false);
                    return RedirectToAction("Index","Home");
                }
                else
                {
                    foreach ( var error in result.Errors)
                    {
                        ModelState.AddModelError("",error.Description);
                    }
                }
            }
            return View(registerVO);
        }
            
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
           
           return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginVO loginVO)
        {
            if (ModelState.IsValid)
            {
            
                var result = await signInManager.PasswordSignInAsync(loginVO.Email, loginVO.Password, loginVO.RememberMe, false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {                   
                      ModelState.AddModelError("", "Login failed!");
                }
            }
            return View(loginVO);
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleVO createRoleVO)
        {

            if (ModelState.IsValid)
            {
                IdentityRole identityRole = new IdentityRole()
                {
                    Name = createRoleVO.Name
                };

                var result = await roleManager.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

            }

            return View(createRoleVO);
        }



        [HttpGet]
        public IActionResult ManageRoles()
        {
            return View(roleManager.Roles);
        }

        [HttpGet]
        public async Task<IActionResult> UserRoleList(string RoleID)
        {
            var role = await roleManager.FindByIdAsync(RoleID);

            if (role == null)
            {
                //if role not found 
                return RedirectToAction("Index", "Home");
            }

            List<UserRoleVO> userRoleVOList = new List<UserRoleVO>();
            foreach (var user in userManager.Users)
            {
                UserRoleVO userRoleVO = new UserRoleVO()
                {
                    Id = user.Id,
                    Name = user.UserName
                };

                if (await userManager.IsInRoleAsync(user, role.Name))
                {
                    userRoleVO.IsSelected = true;
                }
                else
                {
                    userRoleVO.IsSelected = false;

                }
                userRoleVOList.Add(userRoleVO);
            }

            return View(userRoleVOList);
        }


        [HttpPost]
        public async Task<IActionResult> UserRoleList(List<UserRoleVO> model, string RoleID)
        {
            var role = await roleManager.FindByIdAsync(RoleID);

            if (role == null)
            {
                //if role not found 
                return RedirectToAction("Index", "Home");
            }

            for (int i = 0; i < model.Count; i++)
            {
                var user = await userManager.FindByIdAsync(model[i].Id);
                IdentityResult result = null;
                if (model[i].IsSelected && !(await userManager.IsInRoleAsync(user, role.Name)))
                {
                    result = await userManager.AddToRoleAsync(user, role.Name);
                }
                if (!(model[i].IsSelected) && await userManager.IsInRoleAsync(user, role.Name))
                {
                    result = await userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else
                {
                    continue;
                }
            }
            return RedirectToAction("ManageRoles");
        }
    }
}