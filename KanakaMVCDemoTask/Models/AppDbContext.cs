﻿using KanakaMVCDemoTask.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaMVCDemoTask.VO;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace KanakaMVCDemoTask.Models
{
    public class AppDbContext : IdentityDbContext 
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
            
        }

        public DbSet<Student> Student { set; get; }

        public DbSet<KanakaMVCDemoTask.VO.StudentVO> StudentVO { get; set; }
    }
}
