﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaMVCDemoTask.Domain
{
    public class Student
    {
        public long Id { set; get; }
        public string RollNo { set; get; }
        public  string Name { set; get; }
        public int Contact { set; get; }
    }
}
