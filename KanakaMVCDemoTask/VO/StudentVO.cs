﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaMVCDemoTask.VO
{
    public class StudentVO
    {
        public long Id { set; get; }
        public string RollNo { set; get; }
        public string Name { set; get; }
        public int Contact { set; get; }
    }
}
