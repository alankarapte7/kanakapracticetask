﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaMVCDemoTask.VO
{
    public class AddStudentVO
    {
        [Required]
        public string RollNo { set; get; }
        [Required]
        public string Name { set; get; }
        [Required]
        public int Contact { set; get; }
    }
}
