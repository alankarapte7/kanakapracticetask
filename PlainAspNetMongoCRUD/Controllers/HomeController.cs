﻿using MongoDB.Bson;
using PlainAspNetMongoCRUD.Models;
using PlainAspNetMongoCRUD.Repository;
using PlainAspNetMongoCRUD.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlainAspNetMongoCRUD.Controllers
{

    public class HomeController : Controller
    {

        private readonly PersonRepository _personRepository;

        public HomeController()
        {
            this._personRepository = new PersonRepository();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var personViewModelList = _personRepository.GetAllPersons()
                .Select(p => new PersonViewModel { Id = p.Id.ToString(), Name = p.Name, Address = p.Address })
                .ToList<PersonViewModel>();

            return View(personViewModelList);
        }

        [HttpGet]
        public ActionResult AddPerson()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddPerson(AddPersonViewModel person)
        {
            if (ModelState.IsValid)
            {
                _personRepository.AddPerson(new Person
                {
                    Name = person.Name,
                    Address = person.Address
                });

                return RedirectToAction("Index");
            }
            return View(person);
        }

        [HttpGet]
        public ActionResult DeletePerson(string id)
        {
            _personRepository.DeletePerson(id);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult UpdatePerson(string id)
        {
            var person = _personRepository.GetPersonById(id);
            PersonViewModel personViewModel = new PersonViewModel { Id = person.Id.ToString(), Name = person.Name, Address = person.Address };
            return View(personViewModel);
        }


        [HttpPost]
        public ActionResult UpdatePerson(PersonViewModel person)
        {
            if (ModelState.IsValid)
            {
                _personRepository.UpdatePerson(new Person
                {
                    Id = new ObjectId(person.Id),
                    Name = person.Name,
                    Address = person.Address
                });

                return RedirectToAction("Index");
            }
            return View(person);
        }

        [HttpGet]
        public ActionResult DetailPerson(string id)
        {
            var person = _personRepository.GetPersonById(id);
            PersonViewModel personViewModel = new PersonViewModel { Id = person.Id.ToString(), Name = person.Name, Address = person.Address };
            return View(personViewModel);
        }

    } //controller
}