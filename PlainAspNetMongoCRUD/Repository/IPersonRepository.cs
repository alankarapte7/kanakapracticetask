﻿using PlainAspNetMongoCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlainAspNetMongoCRUD.Repository
{
    public interface IPersonRepository
    {
        List<Person> GetAllPersons();
        Person GetPersonById(string id);

        void AddPerson(Person person);

        void UpdatePerson(Person person);

        void DeletePerson(string personId);

    }
}