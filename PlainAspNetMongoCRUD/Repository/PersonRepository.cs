﻿using MongoDB.Bson;
using MongoDB.Driver;
using PlainAspNetMongoCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlainAspNetMongoCRUD.Repository
{
    public class PersonRepository : IPersonRepository
    {

        private readonly IMongoClient client;
        private readonly IMongoDatabase database;
        private readonly IMongoCollection<Person> collection;

        public PersonRepository()
        {
            this.client = new MongoClient("mongodb://localhost:27017");
            this.database = client.GetDatabase("personCRUDDB");
            this.collection = database.GetCollection<Person>("person");

        }

        public List<Person> GetAllPersons()
        {
            return collection.AsQueryable<Person>().ToList();
        }

        public void AddPerson(Person person)
        {
            collection.InsertOne(person);
        }

        public void DeletePerson(string personId)
        {
            collection.DeleteOne(p => p.Id == ObjectId.Parse(personId));
        }

        public void UpdatePerson(Person person)
        {
            collection.UpdateOne(p => p.Id == person.Id,
                    Builders<Person>.Update.Set(p => p.Name, person.Name)
                                           .Set(p => p.Address, person.Address)
            );
        }

        public Person GetPersonById(string id)
        {
            return collection.AsQueryable<Person>().SingleOrDefault(p => p.Id == ObjectId.Parse(id));
        }
    }
}