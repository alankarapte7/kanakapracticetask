﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PlainAspNetMongoCRUD.ViewModels
{
    public class AddPersonViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Address { set; get; }
    }
}