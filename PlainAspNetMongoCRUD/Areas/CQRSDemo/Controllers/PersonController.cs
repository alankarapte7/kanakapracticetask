﻿using PlainAspNetMongoCRUD.Areas.CQRSDemo.Services;
using PlainAspNetMongoCRUD.Models;
using PlainAspNetMongoCRUD.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PlainAspNetMongoCRUD.Areas.CQRSDemo.Controllers
{
    public class PersonController : Controller
    {
        private readonly ICommandService _commandService;
        private readonly IQueryService _queryService;

        public PersonController()
        {
            this._commandService = new CommandService();
            this._queryService = new QueryService();
        }


        [HttpGet]
        public ActionResult AddPerson()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddPerson(AddPersonViewModel person)
        {
            if (ModelState.IsValid)
            {
                _commandService.AddPerson(new Person
                {
                    Name = person.Name,
                    Address = person.Address
                });

                return RedirectToAction("Index","../Home");
            }
            return View(person);
        }

        [HttpGet]
        public ActionResult DetailPerson(string id)
        {
            var person = _queryService.GetPersonById(id);
            PersonViewModel personViewModel = new PersonViewModel { Id = person.Id.ToString(), Name = person.Name, Address = person.Address };
            return View(personViewModel);
        }

    } //controller
}