﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;
using PlainAspNetMongoCRUD.Models;

namespace PlainAspNetMongoCRUD.Areas.CQRSDemo.Services
{
    public class QueryService : IQueryService
    {
        private readonly IMongoClient client;
        private readonly IMongoDatabase database;
        private readonly IMongoCollection<Person> collection;

        public QueryService()
        {
            this.client = new MongoClient("mongodb://localhost:27017");
            this.database = client.GetDatabase("personCRUDDB");
            this.collection = database.GetCollection<Person>("person");
        }

        public Person GetPersonById(string id)
        {
            return collection.AsQueryable<Person>().SingleOrDefault(p => p.Id == ObjectId.Parse(id));
        }
    }
}