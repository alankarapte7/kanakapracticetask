﻿using PlainAspNetMongoCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlainAspNetMongoCRUD.Areas.CQRSDemo.Services
{
    interface ICommandService
    {
        void AddPerson(Person person);
    }
}
