﻿using PlainAspNetMongoCRUD.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlainAspNetMongoCRUD.Areas.CQRSDemo.Services
{
    interface IQueryService
    {
        Person GetPersonById(string id);
    }
}
