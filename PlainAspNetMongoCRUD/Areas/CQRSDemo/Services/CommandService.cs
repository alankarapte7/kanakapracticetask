﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Driver;
using PlainAspNetMongoCRUD.Models;

namespace PlainAspNetMongoCRUD.Areas.CQRSDemo.Services
{
    public class CommandService : ICommandService
    {
        private readonly IMongoClient client;
        private readonly IMongoDatabase database;
        private readonly IMongoCollection<Person> collection;

        public CommandService()
        {
            this.client = new MongoClient("mongodb://localhost:27017");
            this.database = client.GetDatabase("personCRUDDB");
            this.collection = database.GetCollection<Person>("person");
        }

        public void AddPerson(Person person)
        {
            collection.InsertOne(person);
        }
    }
}