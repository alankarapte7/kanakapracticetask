﻿using System.Web.Mvc;

namespace PlainAspNetMongoCRUD.Areas.CQRSDemo
{
    public class CQRSDemoAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "CQRSDemo";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CQRSDemo_default",
                "CQRSDemo/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}