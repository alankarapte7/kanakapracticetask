﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlainAspNetMongoCRUD.Models
{
    public class Person
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement(elementName: "name")]
        public string Name { get; set; }

        [BsonElement(elementName: "address")]
        public string Address { set; get; }
    }
}