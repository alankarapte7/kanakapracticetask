﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaPracticeTask.VO
{
    public class AddEmployeeVO
    {
        [Required]
        public string fname { set; get; }

        [Required]
        public string lname { set; get; }

        [Required]
        public long salary { get; set; }
    }
}
