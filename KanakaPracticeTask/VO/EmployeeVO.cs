﻿using KanakaPracticeTask.Domain;
using KanakaPracticeTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaPracticeTask.VO
{
    public class EmployeeVO
    {
        //public long id { set; get; }
        //public string fname { set; get; }
        //public string lname { set; get; }
        //public long salary { get; set; }

        public Employee employee { get; set; }
    }
}
