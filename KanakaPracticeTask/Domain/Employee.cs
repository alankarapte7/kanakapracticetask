﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaPracticeTask.Domain
{
    public class Employee
    {
        //   public string Name { set { name = Name; } get { return name; } }

        public long id { set; get; }
        public string fname { set; get; }
        public string lname { set; get; }
        public long salary { get; set; }
    }
}
