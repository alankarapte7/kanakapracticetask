﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using KanakaPracticeTask.Domain;
using KanakaPracticeTask.Models;
using KanakaPracticeTask.VO;

namespace KanakaPracticeTask.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        SqlConnection connection = new SqlConnection("Data Source=DESKTOP-1FQPJ3U;Initial Catalog=kanaka_practice;Integrated Security=True");

        public bool AddEmployee(Employee employee)
        {
            using (connection)
            {
                connection.Open();
                int rowAffected = new SqlCommand("INSERT INTO employee(fname, lname, salary, status) VALUES('" + employee.fname + "' , '" + employee.lname + "' ," + employee.salary + ",'A')", connection).ExecuteNonQuery();

                if (rowAffected == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public List<Employee> GetAllEmployees()
        {
            List<Employee> employeeList = new List<Employee>();
            
            using (connection)
            {
                connection.Open();
                //  SqlCommand command = new SqlCommand("insert into Employee(id, fname, lname, salary) values(5, 'alankar', 'apte2', 2000)", connection);
                //  command.ExecuteReader().Close();
                SqlDataReader dataReader = new SqlCommand("SELECT * FROM employee WHERE status = 'A'", connection).ExecuteReader();
                while (dataReader.Read())
                {
                    Employee employee = new Employee();
                   
                    employee.id = dataReader.GetInt64(0);
                    //employee.id = Int64.Parse(dataReader.GetString(0));
                    employee.fname = dataReader.GetString(1);
                    employee.lname = dataReader.GetString(2);
                    employee.salary = dataReader.GetInt64(3);

                    
                    employeeList.Add(employee);
                }
            }
            return employeeList;
        }

        public void DeleteEmployee(int id)
        {
            using (connection)
            {
                connection.Open();
                SqlDataReader dataReader = new SqlCommand("UPDATE employee SET status = 'I' where id =" + id, connection).ExecuteReader();
            }
        }
    }
}
