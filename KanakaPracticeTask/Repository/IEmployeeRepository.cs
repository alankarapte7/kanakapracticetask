﻿
using KanakaPracticeTask.Domain;
using KanakaPracticeTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaPracticeTask.Repository
{
    public interface IEmployeeRepository
    {
         bool AddEmployee(Employee employee);
         List<Employee> GetAllEmployees();
         void DeleteEmployee(int id);
    }
}
