﻿create procedure spInsertUser
	@Username VARCHAR(50),
	@Password VARCHAR(50),
	@Email VARCHAR(50)
as
begin
	set nocount on; --used to avoid affect row msg.
	if exists(select UserId from Users where Username = @Username)
		begin
			select -1; --username already existed
		end
	else if exists(select UserId from Users where Email = @Email)
		begin
			select -2; --email already existed
		end
	else
		begin 
			insert into Users(Username, Email, Password, CreatedDate) values(@Username, @Email, @Password, GETDATE());
			select SCOPE_IDENTITY() --return userID
		end
end