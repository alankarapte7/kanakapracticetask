﻿--NOTE TIP for task: view statements should be single in batch while firing query..so first select it and click execute
--view (every time fire query on base table to get data)
create view [SalaryAboveAvgSalary] 
as
select (fname + lname) as fullName, salary 
from Employee 
where salary > (select AVG(salary) from employee);

--indexed view (have some rule while creating)
--like used schema binding, base table should refernce with 2 part, first create unique clustered index,prefer used joins instead subquery
create view EmployeeFullNameWithSalary with SchemaBinding
as
select (fname+' ' +lname) as full_name from dbo.Employee 

--creating indexed on view (so it only fire query on base table if base table data has change)
create unique clustered index idx_EmployeeFullNameWithSalary_fullName on EmployeeFullNameWithSalary (full_name)

--drop view.
drop view SalaryAboveAvgSalary;