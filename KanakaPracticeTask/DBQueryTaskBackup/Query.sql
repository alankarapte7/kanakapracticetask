﻿CREATE TABLE Employee
(
	id bigint IDENTITY(1,1) NOT NULL,
	fname varchar(50) NULL,
	lname varchar(50) NULL,
	salary bigint NULL,
	status varchar(50) NULL,
 CONSTRAINT PK_Person PRIMARY KEY(id)
);



CREATE TABLE Users
(
	UserId bigint IDENTITY(1,1) NOT NULL,
	Username varchar(50) NOT NULL,
	Password varchar(50) NOT NULL,
	Email varchar(50) NOT NULL,
	CreatedDate datetime NOT NULL,
	LastLoginDate datetime NULL,
 CONSTRAINT PK_Users PRIMARY KEY (UserId) 
);
