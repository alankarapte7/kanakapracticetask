﻿--index make data retriving faster with indexed column
--but slow down DML

--if index not apply on column sql optimization engine used table scan
--if index apply on column sql optimization engine used index scan/seek

--clustered indexed
--primary key by default makes clustered index
--physically arrange table data so only one per table
--sparse index (balanced tree structure) index smaller than actual data
--faster than non clusred coz index & record value at one placed
--example: phone directory 

--non clustered index
--unique key by default makes non clustered index
--logically stored index as 'search key' & 'blocked pointer/row referance'
--can have multiple per table, depends on our need & supported by RDBMS
--slower than clustered index coz index and data at separate place.
--example: book index


--composite index - when we apply index on combination two cloumn

-- Below i have demonstrate some index 
-- >I used table of Adhaar details considered it have huge so we want data retriving faster so i create indexes some column) 

-- index demonstration
-- suppose this table gonna consist be huge data like all state adhaar details
create table AdhaarCardPerson
(
	id bigint null,
	fname varchar(20) not null,
	lname varchar(20) null,
	adhaarNo bigint not null,
	address varchar(20) null
);

--creating non clustered index on id 
create index idx_AdhaarCardPerson_id 
on AdhaarCardPerson (id)

--droping our index
drop index idx_AdhaarCardPerson_id 
on  AdhaarCardPerson

--now 
--creating clustered index on id
create clustered index idx_AdhaarCardPerson_id 
on AdhaarCardPerson (id)

--creating 'composite non clustered index' on fname & lname
create index idx_AdhaarCardPerson_fname_lname 
on  AdhaarCardPerson (fname, lname)
