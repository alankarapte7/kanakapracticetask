﻿--Department 
create table Department 
(
deptId bigint not null,
name varchar(50) not null,
constraint PK_Department PRIMARY KEY (deptId)
);


--Teacher
create table Teacher 
(
id bigint identity(1,1) not null,
name varchar(50) not null,
salary bigint null,
deptId bigint not null,
constraint PK_Teacher PRIMARY KEY (id),
constraint FK_Department Foreign key (deptid ) references Department
);

--inner join: 
--inner join - EQUI JOIN : used equality operatior
select t.name as teacher, t.salary, d.name as department 
from Teacher as t
inner join Department as d 
on t.deptId = d.id;

--inner join to find distinct  record
select distinct(emp1.fname) from Employee as emp1 inner join Employee as emp2 on emp1.fname = emp2.fname ;

--NON-EQUI JOIN : used other than equality operatior
--jcnksckj

--NATURAL JOIN - creates an implicit join clause based on the common columns (name, datatype) in the two tables being joined


--Outer joins:-
--Left JOIN : here i printing no. of teacher per department
select count(Teacher.name) as no_of_teachers , Department.name as Dept_Name
from Teacher  
left join Department 
on Teacher.deptId = Department.id group by Department.name; 


--Left JOIN
select Teacher.name, Department.name
from Teacher  
left join Department 
on Teacher.deptId = Department.id order by Teacher.name;   


--RIGHT JOIN
select Teacher.name, Department.name
from Teacher  
right join Department 
on Teacher.deptId = Department.id order by Teacher.name;