﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using KanakaPracticeTask.Domain;
using KanakaPracticeTask.Models;
using KanakaPracticeTask.Repository;
using KanakaPracticeTask.VO;
using Microsoft.AspNetCore.Mvc;

namespace KanakaPracticeTask.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeRepository employeeRepository;
        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            this.employeeRepository = employeeRepository;
        }

        public IActionResult Index()
        {
            List<EmployeeVO> employeeVOList = new List<EmployeeVO>();



            foreach(Employee employee in employeeRepository.GetAllEmployees())
            {
                employeeVOList.Add(new EmployeeVO
                {
                    employee = employee
                });
            }

            return View("ShowEmployees", employeeVOList);
        }

        [HttpGet]
        public IActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddEmployee(AddEmployeeVO addEmployeeVO)
        {
            if (ModelState.IsValid)
            {
                Employee employee = new Employee {
                    fname = addEmployeeVO.fname,
                    lname = addEmployeeVO.lname,
                    salary = addEmployeeVO.salary
                };
                if (employeeRepository.AddEmployee(employee))
                {
                    TempData["operation"] = "create";
                }
                else
                {
                    TempData["operation"] = "createError";
                }


                return RedirectToAction("Index");
            }
            return View(addEmployeeVO);
            
        }

        

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult DeleteEmployee(int id)
        {
            employeeRepository.DeleteEmployee(id);

            TempData["operation"] = "delete";

            return RedirectToAction("Index");
        }
    }
}