﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaAuthntcAndAuthrztnTask.VO
{

    public class LoginUserVO
    {
        [Required]
        [EmailAddress]
        public string Email { set; get; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { set; get; }

        [Required]
        [Display(Name = "Remember Me")]
        public bool RememberMe { set; get; }
    }
}
