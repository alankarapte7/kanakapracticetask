﻿using KanakaAuthntcAndAuthrztnTask.VO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaAuthntcAndAuthrztnTask.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;

        public AdminController(RoleManager<IdentityRole> roleManager,
                               UserManager<IdentityUser> userManager)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleVO createRoleVO)
        {
            if (ModelState.IsValid)
            {
                IdentityRole identityRole = new IdentityRole() { Name = createRoleVO.Name };

                IdentityResult result = await roleManager.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }

                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("", item.Description);
                }
            }


            return View(createRoleVO);
        }

        [HttpGet]
        public IActionResult ShowRoles()
        {
            return View(roleManager.Roles);

        }

        [HttpGet]
        public async Task<IActionResult> UsersList(string RoleID)
        {
            var role = await roleManager.FindByIdAsync(RoleID);

            if (role == null)
            {
                //if role not found 
                return RedirectToAction("Index", "Home");
            }

            List<UsersFromRoleVO> model = new List<UsersFromRoleVO>();
            foreach (var user in userManager.Users)
            {
                UsersFromRoleVO usersFromRoleVO = new UsersFromRoleVO()
                {
                    Id = user.Id,
                    Name = user.UserName
                };

                if (await userManager.IsInRoleAsync(user, role.Name))
                {
                    usersFromRoleVO.IsSelected = true;
                }
                else
                {
                    usersFromRoleVO.IsSelected = false;

                }
                model.Add(usersFromRoleVO);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UsersList(List<UsersFromRoleVO> model, string RoleID)
        {
            var role = await roleManager.FindByIdAsync(RoleID);

            if (role == null)
            {
                //if role not found 
                return RedirectToAction("Index", "Home");
            }

            for (int i = 0; i < model.Count; i++)
            {
                var user = await userManager.FindByIdAsync(model[i].Id);
                IdentityResult result = null;
                if (model[i].IsSelected && !(await userManager.IsInRoleAsync(user, role.Name)))
                {
                    result = await userManager.AddToRoleAsync(user, role.Name);
                }
                if (!(model[i].IsSelected) && await userManager.IsInRoleAsync(user, role.Name))
                {
                    result = await userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else
                {
                    continue;
                }
            }
            return RedirectToAction("ShowRoles");
        }

    }
}
