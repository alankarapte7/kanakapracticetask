﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using KanakaAuthntcAndAuthrztnTask.VO;

namespace KanakaAuthntcAndAuthrztnTask.Controllers
{

    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        public AccountController(UserManager<IdentityUser> userManager,
                                 SignInManager<IdentityUser> signInManager)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        public SignInManager<Microsoft.AspNetCore.Identity.IdentityUser> SignInManager { get; }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterUserVO registerVO)
        {
            if (ModelState.IsValid)
            {
                var user = new IdentityUser
                {
                    Email = registerVO.Email,
                    UserName = registerVO.Email,
                };
                var result = await userManager.CreateAsync(user, registerVO.Password);
                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                foreach (var item in result.Errors)
                {
                    ModelState.AddModelError("", item.Description);
                }
            }
            return View(registerVO);
        }

        [HttpGet]
        public ViewResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginUserVO loginUserVO)
        {
            if (ModelState.IsValid)
            {

                var result = await signInManager.PasswordSignInAsync(
                    loginUserVO.Email,
                    loginUserVO.Password,
                    loginUserVO.RememberMe,
                    false
                    );
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                ModelState.AddModelError("", "Invalid login");
            }
            return View(loginUserVO);
        }

        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }

}