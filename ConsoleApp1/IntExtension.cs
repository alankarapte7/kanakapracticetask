﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    public static class IntExtension
    {
        public static int reverseMe(this int num)
        {
            int remainder = 0, reverse = 0;
            while(num > 0)
            {
                remainder = num % 10;
                reverse = (reverse * 10) + remainder;
                num = num / 10;
            }
            return reverse;
        }
    }
}
