﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;


namespace SerializationExample2
{
    class Employee
    {
        //   public string Name { set { name = Name; } get { return name; } }
        public string fname { set; get; }
        public string lname { set; get; }
        public int salary { get; set; }


        //public static void SerializeByJSS()
        //{
        //    Employee employee = new Employee { fname = "alankar", lname = "apte", salary = 2000 };
        //    FileStream fileStream = new FileStream("H:\\data.txt", FileMode.OpenOrCreate);    
        //}


        public static void SerializeByNewtonSoftJSON()
        {
            Console.WriteLine("NewtonSoft.JSON Demo: (add item to list)");
            List<Employee> employees = new List<Employee>();
            do
            {
                Console.Write("\nEnter first name: ");
                string fname = Console.ReadLine();
                Console.Write("Enter last name: ");
                string lname = Console.ReadLine();
                Console.Write("Enter salry: ");
                
                try
                {
                    int salary = Int32.Parse(Console.ReadLine());
                    employees.Add(new Employee { fname = fname, lname = lname, salary = salary });
                }
                catch (FormatException)
                {
                    Console.WriteLine("invalid salary");
                }
               
                Console.WriteLine("*Type 'y' to add another employee");
            } while (Console.ReadKey().Key.ToString().Trim().ToLower().Equals("y"));
            string employeesJSON = JsonConvert.SerializeObject(employees);
            File.WriteAllText("H:\\data2.txt", employeesJSON);
            Console.WriteLine("JSON successfully saved");
        }

        public static List<Employee> DeserializeByNewtonSoftJSON()
        {
            Object obj = JsonConvert.DeserializeObject(File.ReadAllText("H:\\data2.txt"));
            Console.WriteLine(obj);       
            
            return JsonConvert.DeserializeObject<List<Employee>>(File.ReadAllText("H:\\data2.txt"));
        }

    }
}
