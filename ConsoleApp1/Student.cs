﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace SerializationExample
{
    [Serializable]
    class Student
    {
        public string Name { set; get; } //easy way of getter - setter
        private int RollNo { set; get; } //easy way of getter - setter
        [NonSerialized]
        private string address;

        //normal way of getter - setter
        public string Address
        {
            set { address = Address; }
            get { return address; }
        }

        public void print(Student student)
        {
            Console.WriteLine("name: " + student.Name + ", roll:" + student.RollNo + ", address: " + student.address);
        }

        public static void SerializeData(string name, int rollNo, string address)
        {
            Student student = new Student();
            student.Name = name;
            student.RollNo = rollNo;
            student.address = address;

            student.print(student);

            FileStream fileStream = new FileStream("H:\\data.txt", FileMode.OpenOrCreate);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, student);

            fileStream.Close();
            Console.WriteLine("data serialize successfully");
        }

        public static void DeserializeData()
        {
            FileStream fileStream = new FileStream("H:\\data.txt", FileMode.Open);

            BinaryFormatter binaryFormatter = new BinaryFormatter();

            Student student = (Student)binaryFormatter.Deserialize(fileStream);

            Console.Write("data is ");
            student.print(student);
            fileStream.Close();
        }

        
    }
}
