﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    //GC concept demo
    class GarbageCollectionDemo
    {
        //demo on Generation traversing of object
        public void GCDemo1()
        {            
            //here i printing total generation support by this system
            Console.WriteLine("no. of max generation support by this system: " + GC.MaxGeneration);

            //here i printing generation of object before GC and after GC
            String obj = new String("obj of string");
            Console.WriteLine("Generation no of string obj before GC: " + GC.GetGeneration(obj));
            GC.Collect(); //GC for all generations
            Console.WriteLine("Generation no of string obj after 1st GC: " + GC.GetGeneration(obj));
            GC.Collect();
            Console.WriteLine("Generation no of string obj after 2nd GC: " + GC.GetGeneration(obj));
            GC.Collect(); 
            Console.WriteLine("Generation no of string obj after 3rd GC: " + GC.GetGeneration(obj));
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));
        }

        //demo on no. of times GC occure per generation
        public void GCDemo2()
        {
            //here i explicitly occuring GC for each generation and print no. of time GC occur per generation
            Console.WriteLine(">GC not done yet");
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            GC.Collect(); //GC for all generations
            Console.WriteLine(">GC for all generations done");
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            GC.Collect(0); //GC for upto generation 0
            Console.WriteLine(">GC upto generation 0 done");
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            GC.Collect(1); //GC for upto generation 1
            Console.WriteLine(">GC upto generation 1 done");
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            GC.Collect(2); //GC for upto generation 2
            Console.WriteLine(">GC upto upto generation 2 done");
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            GC.Collect(); //GC for all generations
            Console.WriteLine(">GC for all generations done");
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));            
        }

        //demo on memory allocation before & after GC
        public void GCDemo3()
        {
            String obj = new String("a");
            Console.WriteLine("No of bytes allocated in system before GC: " + GC.GetTotalMemory(false));

            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            Console.WriteLine("No of bytes allocated in system after GC: "+ GC.GetTotalMemory(true));

            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            Console.WriteLine("\n");
            //No of bytes allocated in system without GC 
            for (int i =0; i<1000; i++)
            {
                
                GC.KeepAlive(new String("a"));
                if (i==999)
                {
                    Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
                    Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
                    Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));
                    Console.WriteLine(">No of bytes allocated in system without GC: " + GC.GetTotalMemory(false));
                }
            }
            Console.WriteLine(">No bytes allocated in system before GC (outside for loop): " + GC.GetTotalMemory(false));
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));

            Console.WriteLine("\n");
            //No of bytes allocated in system with GC 
            for (int i = 0; i < 1000; i++)
            {
                GC.GetTotalMemory(true);
                
                // GC.KeepAlive(new String("a"));
                if (i == 999)
                {
                    Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
                    Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
                    Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));
                    Console.WriteLine("No bytes allocated in system with GC: " + GC.GetTotalMemory(true));

                }
            }
            Console.WriteLine(">No bytes allocated in system before GC (outside for loop): " + GC.GetTotalMemory(false));
            Console.WriteLine("no of times GC done in Generation 0: " + GC.CollectionCount(0));
            Console.WriteLine("no of times GC done in Generation 1: " + GC.CollectionCount(1));
            Console.WriteLine("no of times GC done in Generation 2: " + GC.CollectionCount(2));
        }      
    }
}
