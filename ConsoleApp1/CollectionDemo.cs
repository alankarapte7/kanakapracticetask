﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace ConsoleApp1
{
    class CollectionDemo
    {
        public void ArrayListDemo()
        {
            Console.WriteLine(">\n>\n>>>>>>>>> ArrayListDemo <<<<<<<<<<\n<\n<");

            ArrayList arrayList = new ArrayList(3); //we have declare initial default capacity in constructor

            Console.WriteLine("capacity before adding extra element than declared capacity: " + arrayList.Capacity);

            arrayList.Add("alankar");
            arrayList.Add("raghav");
            arrayList.Add("mangesh");
            arrayList.Add("balu");
            arrayList.Add("amar");

            //arrayList.Remove("sham");
            //arrayList.RemoveAt(1);
            //arrayList.RemoveRange(1,3);

            Console.WriteLine("capacity after adding extra element than declared capacity: " + arrayList.Capacity);
            arrayList.TrimToSize();
            Console.WriteLine("capacity after fn - TrimToSize adding extra element than declared capacity: " + arrayList.Capacity);
            Console.WriteLine("count: " + arrayList.Count);

            arrayList.Sort();
            arrayList.Reverse();

            Console.WriteLine(">>printing items in ArrayList: ");
            foreach (Object item in arrayList)
            {
                Console.WriteLine("\t"+item);
            }
        }

        public void StackDemo()
        {
            Console.WriteLine(">\n>\n>>>>>>>>> StackDemo <<<<<<<<<<\n<\n<");

            Stack stack = new Stack();
            // stack & queue dont sort beacause it is LIFO & FIFO respectively so it must have to maintain sequence

            stack.Push("a");
            stack.Push("b");
            stack.Push("c");
            stack.Push("d");
            stack.Push("e");

            //peek return top element
            Console.WriteLine("top element before fn Pop: "+stack.Peek());

            stack.Pop();
            Console.WriteLine("top element after fn Pop: " + stack.Peek());

            Console.WriteLine(">>printing items in Stack: ");
            foreach (Object item in stack)
            {
                Console.WriteLine("\t" + item);
            }
        }

        public void QueueDemo()
        {
            Console.WriteLine(">\n>\n>>>>>>>>> QueueDemo <<<<<<<<<<\n<\n<");

            Queue queue = new Queue();
            // stack & queue dont sort beacause it is LIFO & FIFO respectively so it must have to maintain sequence

            queue.Enqueue("a");
            queue.Enqueue("b");
            queue.Enqueue("c");
            queue.Enqueue("d");
            queue.Enqueue("e");


            //peek returns first element
            Console.WriteLine("top element before fn Dequeue: " + queue.Peek());
            queue.Dequeue(); //FIFO -so remove first element
            Console.WriteLine("top element after fn Dequeue: " + queue.Peek());

            Console.WriteLine(">>printing items in Queue: ");
            foreach (Object item in queue)
            {
                Console.WriteLine("\t" + item);
            }
        }

        public void HashtableDemo()
        {
            Console.WriteLine(">\n>\n>>>>>>>>> HashtableDemo <<<<<<<<<<\n<\n<");

            Hashtable hashtable = new Hashtable();

            //here i adding in unsorted order so demonstrate that Hashable not by default sorted by key like SortedList
            hashtable.Add("205", "alankar");
            hashtable.Add("203", "sham");
            hashtable.Add("202", "dinesh");
            hashtable.Add("201", "rahul");
            hashtable.Add("204", "bhau");

            //  hashtable.ContainsValue("rahul");
            //hashtable.ContainsKey("204")

            Console.WriteLine("checking element before fn Remove: " + hashtable.Contains("204"));
            hashtable.Remove("204");
            Console.WriteLine("checking element after fn Remove: " + hashtable.Contains("204"));

            ICollection icollection = hashtable.Keys;
            Console.WriteLine(">>printing keys in Hashtable: ");
            foreach (Object item in icollection)
            {
                Console.WriteLine("\t" + item);
            }

            Console.WriteLine(">>printing keys-value in Hashtable using 'DictionaryEntry' struct in c#: ");
            foreach (DictionaryEntry item in hashtable)
            {
                Console.WriteLine("\tkey: " + item.Key + " | value: "+ item.Value);
            }
        }

        public void SortedListDemo()
        {
            Console.WriteLine(">\n>\n>>>>>>>>> SortedListDemo <<<<<<<<<<\n<\n<");

            SortedList sortedList = new SortedList();

            //here i adding in unsorted order so demonstrate that SortedList by default sorted by key unlike Hashable 
            sortedList.Add("205", "alankar");
            sortedList.Add("203", "sham");
            sortedList.Add("202", "dinesh");
            sortedList.Add("201", "rahul");
            sortedList.Add("204", "bhau");

            //sortedList.ContainsValue("rahul");
           // sortedList.ContainsKey("204")

            Console.WriteLine("checking element before fn Remove: " + sortedList.Contains("204"));
            sortedList.Remove("204");
            Console.WriteLine("checking element after fn Remove: " + sortedList.Contains("204"));

            ICollection icollection = sortedList.Keys;
            Console.WriteLine(">>printing keys in SortedList: ");
            foreach (Object item in icollection)
            {
                Console.WriteLine("\t" + item);
            }

            //sortedList.IndexOfKey("202");
            //sortedList.GetByIndex(2);

            Console.WriteLine(">>printing keys-value in SortedList using 'DictionaryEntry' struct in c#: ");
            foreach (DictionaryEntry item in sortedList)
            {
                Console.WriteLine("\tkey: " + item.Key + " | value: " + item.Value);
            }
        }
    }
}
