﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExceptionHandlingExample
{
    class AddNumber
    {
        public static int Division(int num1, int num2)
        {
            try
            {
                var result = num1 / num2;
                return result;
            }
            catch (FormatException)
            {
                Console.WriteLine("num not valid");
                return 0;
            }
            catch (Exception)
            {
                Console.WriteLine("divisor cant be zero");
                return 0;
            }

        }
    }
}
