﻿using ExceptionHandlingExample;
using Newtonsoft.Json;
using SerializationExample;
using SerializationExample2;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            //    Console.WriteLine("Serialization started: ");
            //    Console.Write("Enter name: ");
            //    string name = Console.ReadLine();
            //    Console.Write("Enter roll no: ");
            //    try
            //    {
            //        int rollNo = Int32.Parse(Console.ReadLine());
            //        Console.Write("Enter address(this property is nonserialize): ");
            //        string address = Console.ReadLine();
            //        Student.SerializeData(name, rollNo, address);
            //    }
            //    catch (FormatException fe)
            //    {
            //        Console.WriteLine("rollNo not valid");
            //    }

            //   Console.WriteLine("Deserialization started: ");
            //   Student.DeserializeData();
            //   Console.WriteLine(">>>>>>>><<<<<<");

            //Console.WriteLine("Exception handling started");
            //Console.Write("Enter divident: ");
            //int divident = Int32.Parse(Console.ReadLine());
            //Console.Write("Enter divisor: ");
            //int divisor = Int32.Parse(Console.ReadLine());
            //Console.WriteLine("result: " + AddNumber.Division(divident, divisor));

            //    Console.WriteLine(">>>>>>>><<<<<<<");

            //Employee.SerializeByNewtonSoftJSON();

            //List<Employee> employees = Employee.DeserializeByNewtonSoftJSON();

            //foreach (Employee employee in employees)
            //{
            //    Console.WriteLine("Name: " + employee.fname + " " + employee.lname + ", salary: " + employee.salary);
            //}

            //Console.WriteLine("<<<<<<<<<<<Collection Demo>>>>>>>");
            //CollectionDemo collectionDemo = new CollectionDemo();

            // collectionDemo.ArrayListDemo();
            //collectionDemo.StackDemo();
            //collectionDemo.QueueDemo();
            // collectionDemo.HashtableDemo();
            //collectionDemo.SortedListDemo();

            GarbageCollectionDemo garbageCollectionDemo = new GarbageCollectionDemo();
            //garbageCollectionDemo.GCDemo1();
            //garbageCollectionDemo.GCDemo2();
            // garbageCollectionDemo.GCDemo3();

            //extension method example
            int num = 12345;
            int reverse = num.reverseMe();
            Console.WriteLine("num: " + num + ", reverse is: " + reverse);

            Console.ReadLine();
        }
        

        
    }
}
