﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Domain
{
    [Table("Event")]
    public class Event
    {
        [Key]
        public long Id { get; set; }
        public string StartPoint { get; set; }
        public string EndPoint { get; set; }

        [DisplayFormat(DataFormatString ="{0:d}")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
       
        public long BusId { get; set; }
        [ForeignKey("BusId")]
        public Bus Bus { get; set; }
        
        public ICollection<Ticket> Ticket { get; set; }

        
    }
}
