﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Domain
{
    [Table("Bus")]
    public class Bus
    {
        [Key]
        public long Id { set; get; }
        public string Name { get; set; }
        public string Number { get; set; }

        public ICollection<Event> events { get; set; } 
    }
}
