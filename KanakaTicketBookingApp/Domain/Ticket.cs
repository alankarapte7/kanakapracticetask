﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Domain
{
    [Table("Ticket")]
    public class Ticket
    {
        [Key]
        public Guid Id { get; set; }
        public long SeatNo { get; set; }
        public string PassangerName{get; set;}

        public long EventId { get; set; }
        [ForeignKey("EventId")]
        public Event Event { get; set; }
    }
}
