﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    public class EventVO
    {
        public long Id { get; set; }
        public string StartPoint { get; set; }
        public string EndPoint { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }


        public long BusId { get; set; }
        public BusVO BusVO { get; set; }

        public ICollection<TicketVO> TicketsVO { get; set; }
    }
}
