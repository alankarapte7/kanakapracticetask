﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    public class AddEventVO
    {
        [Required]
        public string StartPoint { get; set; }
        [Required]
        public string EndPoint { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:d}")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }


        public long BusId { get; set; }
    }
}
