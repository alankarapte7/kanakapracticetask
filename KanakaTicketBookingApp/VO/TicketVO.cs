﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    public class TicketVO
    {
        public string Id { get; set; }
        public long SeatNo { get; set; }
        public string PassangerName { get; set; }

        public long EventId { get; set; }
        public EventVO EventVO { get; set; }
    }
}
