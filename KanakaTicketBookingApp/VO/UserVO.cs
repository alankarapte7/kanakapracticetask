﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    public class UserVO
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }

    }
}
