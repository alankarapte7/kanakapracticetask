﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    public class UserClaimVO
    {
        public string UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public Claim Claim    { get; set; }


        public bool IsSelected { get; set; }
    }
}
