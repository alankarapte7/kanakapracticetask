﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    
        public class UserRoleVO
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public bool IsSelected { get; set; }
        }
    
}
