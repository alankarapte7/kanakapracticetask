﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    public class BookTicketVO
    {
        public string Id { get; set; }


        public List<bool> SeatNo{ get; set; }


        [Required]
        public string PassangerName { get; set; }

        public long EventId { get; set; }
        public EventVO EventVO { get; set; }
    }
}
