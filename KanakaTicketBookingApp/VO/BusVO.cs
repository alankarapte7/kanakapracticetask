﻿using KanakaTicketBookingApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.VO
{
    public class BusVO
    {
        public long Id { set; get; }
        public string Name { get; set; }
        public string Number { get; set; }

        public ICollection<EventVO> EventsVO { get; set; }
    }
}
