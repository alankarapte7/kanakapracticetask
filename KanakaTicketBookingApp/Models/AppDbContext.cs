﻿using KanakaTicketBookingApp.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaTicketBookingApp.VO;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace KanakaTicketBookingApp.Models
{
    public class AppDbContext : IdentityDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Bus> Bus { get; set; }
        public DbSet<Event> Event { get; set; }
        public DbSet<Ticket> Ticket { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            builder.Entity<IdentityUser>().HasData(
                    new IdentityUser()
                    {
                       
                        Email = "admin@gmail.com",
                        UserName = "admin@gmail.com",
                        PasswordHash = "AQAAAAEAACcQAAAAEI9N4j1bm1MX+D/O9UaYn/DDhXTnxZWvqt1lTiP2bvT/Cw8zppuGjOq9GNM/jTAPuw==",
                    }
                );
            builder.Entity<IdentityRole>().HasData(
                   new IdentityRole()
                   {
                      Name="admin"
                   }
               );
             


        }
        




    }
}
