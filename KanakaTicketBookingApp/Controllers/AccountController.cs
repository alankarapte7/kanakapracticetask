﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using KanakaTicketBookingApp.Models;
using KanakaTicketBookingApp.VO;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KanakaTicketBookingApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly ILogger<AccountController> logger;


        public AccountController(SignInManager<IdentityUser> signInManager,
                                    RoleManager<IdentityRole> roleManager,
                                    UserManager<IdentityUser> userManager,
                                     ILogger<AccountController> logger)
        {
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this.userManager = userManager;
            this.logger = logger;
        }

        [HttpGet]
        public IActionResult Register()
        {
            logger.LogInformation("Register started");
            logger.LogInformation("Register ended");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterVO registerVO)
        {
            logger.LogInformation("Register started", registerVO);

            if (ModelState.IsValid)
            {
                var user = new IdentityUser()
                {
                    Email = registerVO.Email,
                    UserName = registerVO.Email
                };

                var result = await userManager.CreateAsync(user, registerVO.Password);

                if (result.Succeeded)
                {
                    await signInManager.SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }
            }
            logger.LogInformation("Register ended");
            return View(registerVO);
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            logger.LogInformation("Logout started");
            await signInManager.SignOutAsync();
            logger.LogInformation("Logout ended");
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            logger.LogInformation("Login started");
            logger.LogInformation("Login ended");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginVO loginVO)
        {
            logger.LogInformation("Login started", loginVO);

            if (ModelState.IsValid)
            {

                var result = await signInManager.PasswordSignInAsync(loginVO.Email, loginVO.Password, loginVO.RememberMe, false);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Login failed!");
                }
            }
            logger.LogInformation("Login started");

            return View(loginVO);
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            logger.LogInformation("CreateRole started");
            logger.LogInformation("CreateRole ended");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleVO createRoleVO)
        {
            logger.LogInformation("CreateaRole started", createRoleVO);

            if (ModelState.IsValid)
            {
                IdentityRole identityRole = new IdentityRole()
                {
                    Name = createRoleVO.Name
                };

                var result = await roleManager.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

            }
            logger.LogInformation("CreateaRole ended");

            return View(createRoleVO);
        }



        [HttpGet]
        public IActionResult ManageRoles()
        {
            logger.LogInformation("ManageRoles started");
            logger.LogInformation("ManageRoles ended");

            return View(roleManager.Roles);
        }

        [HttpGet]
        public async Task<IActionResult> UserRoleList(string RoleID)
        {
            logger.LogInformation("ManageRoles started",RoleID);

            var role = await roleManager.FindByIdAsync(RoleID);

            if (role == null)
            {
                //if role not found 
                return RedirectToAction("Index", "Home");
            }

            List<UserRoleVO> userRoleVOList = new List<UserRoleVO>();
            foreach (var user in userManager.Users)
            {
                UserRoleVO userRoleVO = new UserRoleVO()
                {
                    Id = user.Id,
                    Name = user.UserName
                };

                if (await userManager.IsInRoleAsync(user, role.Name))
                {
                    userRoleVO.IsSelected = true;
                }
                else
                {
                    userRoleVO.IsSelected = false;

                }
                userRoleVOList.Add(userRoleVO);
            }
            logger.LogInformation("ManageRoles ended");

            return View(userRoleVOList);
        }


        [HttpPost]
        public async Task<IActionResult> UserRoleList(List<UserRoleVO> model, string RoleID)
        {
            logger.LogInformation("UserRoleList started", model, RoleID);

            var role = await roleManager.FindByIdAsync(RoleID);

            if (role == null)
            {
                //if role not found 
                return RedirectToAction("Index", "Home");
            }

            for (int i = 0; i < model.Count; i++)
            {
                var user = await userManager.FindByIdAsync(model[i].Id);
                IdentityResult result = null;
                if (model[i].IsSelected && !(await userManager.IsInRoleAsync(user, role.Name)))
                {
                    result = await userManager.AddToRoleAsync(user, role.Name);
                }
                if (!(model[i].IsSelected) && await userManager.IsInRoleAsync(user, role.Name))
                {
                    result = await userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else
                {
                    continue;
                }
            }
            logger.LogInformation("UserRoleList ended");
            return RedirectToAction("ManageRoles");
        }

        [HttpGet]
        public IActionResult ManageClaim()
        {
            logger.LogInformation("ManageClaim started");

            var users = userManager.Users;
            List<UserVO> usersVO = new List<UserVO>();
            foreach (var user in users)
            {
                usersVO.Add(new UserVO() {
                     Id = user.Id,
                     Email = user.Email,
                     UserName = user.UserName
                });
            }
            logger.LogInformation("ManageClaim ended");

            return View(usersVO);
        }

        [HttpGet]
        public async Task<IActionResult> UserClaimList(string id)
        {
            logger.LogInformation("UserClaimList started",id);

            var user = await userManager.FindByIdAsync(id);
           

            var existingUserClaims = await userManager.GetClaimsAsync(user);
            List<UserClaimVO> userClaimVOs = new List<UserClaimVO>();

            
            foreach (Claim claim in AppClaim.ourClaims)
            {
                UserClaimVO userClaimVO = new UserClaimVO
                {
                    UserId = id,
                    Claim= claim,
                    ClaimType =claim.Type,
                    ClaimValue = claim.Value
                };
                if (existingUserClaims.Any(c => c.Type == claim.Type))
                {

                    userClaimVO.IsSelected = true;
                    
                    
                   
                }
                else
                {
                userClaimVO.IsSelected = false;

            }
            userClaimVOs.Add(userClaimVO);

            }
            logger.LogInformation("UserClaimList ended");

            return View(userClaimVOs);
        }




        [HttpPost]
        public async Task<IActionResult> UserClaimList(List<UserClaimVO> userClaimVOs, string id)
        {
            logger.LogInformation("UserClaimList started", userClaimVOs, id);

            var user = await userManager.FindByIdAsync(id);
            var claims = await userManager.GetClaimsAsync(user);
            var result = await userManager.RemoveClaimsAsync(user, claims);

            if (!result.Succeeded)
            {
                ModelState.AddModelError("","somthing wrong");
                return View(userClaimVOs);
            }
            result = await userManager.AddClaimsAsync(user, userClaimVOs.Where(item =>
                   item.IsSelected).Select(item =>
                      new Claim(item.ClaimType, item.ClaimValue))
               );

            if (!result.Succeeded)
            {
                ModelState.AddModelError("", "claim adding failed");
                return View(userClaimVOs);
            }
            logger.LogInformation("UserClaimList ended");
                return RedirectToAction("ManageClaim");
        }


    }
}