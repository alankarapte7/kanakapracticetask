﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaTicketBookingApp.Domain;
using KanakaTicketBookingApp.Repository;
using KanakaTicketBookingApp.VO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KanakaTicketBookingApp.Controllers
{
    public class BusController : Controller
    {
        private readonly IBusRepository busRepository;
        private readonly ILogger<BusController> logger;

        public BusController(IBusRepository busRepository, ILogger<BusController> logger)
        {
            this.busRepository = busRepository;
            this.logger = logger;
        }
        [HttpGet]
        public IActionResult Index()
        {
            logger.LogInformation("Index started");

            var busesVO = busRepository.GetAllBuses().Select<Bus, BusVO>(bus => new BusVO()
            {
                Id = bus.Id,
                Name=bus.Name,
                Number = bus.Number
            });
            logger.LogInformation("Index ended");

            return View(busesVO);
        }

        [HttpGet]
        public IActionResult AddBus()
        {
            logger.LogInformation(" AddBus started");
            logger.LogInformation(" AddBus ended");

            return View();
        }

        [HttpPost]
        public IActionResult AddBus(AddBusVO addBusVO)
        {
            logger.LogInformation("AddBus started", addBusVO);

            if (ModelState.IsValid)
            {
                Bus bus = new Bus()
                {
                    Name = addBusVO.Name,
                    Number = addBusVO.Number
                };

                busRepository.AddBus(bus);
                logger.LogInformation("AddBus ended");
                return RedirectToAction("Index");
            }
            logger.LogInformation("AddBus ended");

            return View(addBusVO);
        }


    }
}