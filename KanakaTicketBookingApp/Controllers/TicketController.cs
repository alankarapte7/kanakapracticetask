﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaTicketBookingApp.Domain;
using KanakaTicketBookingApp.Repository;
using KanakaTicketBookingApp.VO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KanakaTicketBookingApp.Controllers
{
    public class TicketController : Controller
    {

        private readonly IEventRepository eventRepository;
        private readonly IBusRepository busRepository;
        private readonly ITicketRepository ticketRepository;
        private readonly ILogger<TicketController> logger;

        public TicketController(IEventRepository eventRepository, 
                                IBusRepository busRepository,
                                ITicketRepository ticketRepository,
                                 ILogger<TicketController> logger)
        {
            this.eventRepository = eventRepository;
            this.busRepository = busRepository;
            this.ticketRepository = ticketRepository;
            this.logger = logger;
        }

        [Authorize(Policy = "ManagerRights")]
        public IActionResult Index()
        {
            logger.LogInformation("Index started");

            var ticketsVO = ticketRepository.GetAllTickets().Select<Ticket, TicketVO>(ticket =>
            new TicketVO()
            {
                PassangerName = ticket.PassangerName,
                EventId = ticket.EventId,
                SeatNo = ticket.SeatNo,

                EventVO = new EventVO()
                {
                    StartPoint = ticket.Event.StartPoint,
                    EndPoint = ticket.Event.EndPoint,
                    Date = ticket.Event.Date,
                    BusVO = new BusVO()
                    {
                        Name = busRepository.GetById(ticket.Event.BusId).Name
        }
                }
            });


            logger.LogInformation("Index ended");

            return View(ticketsVO);
        }


        [HttpGet]
        public IActionResult BookTicket(long id)
        {
            logger.LogInformation("BookTicket started",id);


            List<bool> seatStatus = new List<bool>();


                var tickets = ticketRepository.GetTicketByEvent(id);

                for (int i = 0; i < 36; i++)
                {
                    int flag = 0;
                    foreach (var ticket in tickets)
                    {
                        if ((ticket.SeatNo - 1).Equals(i))
                        {
                            seatStatus.Add(true);
                            flag = 1;
                            break;
                        }
                    }

                    if (flag == 0)
                    {
                        seatStatus.Add(false);
                    }
                }
                var bookTicketVO = new BookTicketVO()
                {
                    EventId = id,
                    SeatNo = seatStatus
                };
            logger.LogInformation("BookTicket Ended");

            return View(bookTicketVO);
            
            
        }


        [HttpPost]
        public IActionResult BookTicket(BookTicketVO bookTicketVO)
        {
            logger.LogInformation("Bookticket started", bookTicketVO);

            if (ModelState.IsValid)
            {
                List<long> newSeatNo = new List<long>();
                for (int i = 0; i < bookTicketVO.SeatNo.Count; i++)
                {
                    if (bookTicketVO.SeatNo[i])
                    {
                        newSeatNo.Add((i + 1));
                    }
                }

                foreach (var item in ticketRepository.GetTicketByEvent(bookTicketVO.EventId))
                {
                    if (newSeatNo.Contains(item.SeatNo))
                    {
                        newSeatNo.Remove(item.SeatNo);                       
                    }
                }

                foreach (var item in newSeatNo)
                {
                    Ticket ticket = new Ticket()
                    {
                        EventId = bookTicketVO.EventId,
                        PassangerName = bookTicketVO.PassangerName,
                        SeatNo = item
                    };

                ticketRepository.AddTicket(ticket);
                }
                logger.LogInformation("Bookticket ended"); 
                return RedirectToAction("Index");


            }
            logger.LogInformation("Bookticket ended");
            return View(bookTicketVO);
        }


       
    }
}