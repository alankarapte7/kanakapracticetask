﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaTicketBookingApp.Domain;
using KanakaTicketBookingApp.Repository;
using KanakaTicketBookingApp.VO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace KanakaTicketBookingApp.Controllers
{
    public class EventController : Controller
    {
        private readonly IEventRepository eventRepository;
        private readonly IBusRepository busRepository;
        private readonly ILogger<EventController> logger;

        public EventController(IEventRepository eventRepository, 
                                IBusRepository busRepository, 
                                ILogger<EventController> logger)
        {
            this.eventRepository = eventRepository;
            this.busRepository = busRepository;
            this.logger = logger;
        }
        public IActionResult Index()
        {
            logger.LogInformation("Index started");

            var r = eventRepository.GetAllEvents();

            var eventsVO = eventRepository.GetAllEvents().Select<Event, EventVO>(_event =>
            new EventVO()
            {
                Id = _event.Id,
                StartPoint = _event.StartPoint,
                EndPoint = _event.EndPoint,
                Date = _event.Date,
                
                BusVO = new BusVO()
                {
                    Id = _event.Bus.Id,
                    Name = _event.Bus.Name,
                    Number = _event.Bus.Number
                }
            });
            logger.LogInformation("Index ended");

            return View(eventsVO);
        }

        [HttpGet]
        public IActionResult AddEvent()
        {
            logger.LogInformation("AddEvent started");

            ViewBag.BusVO = busRepository.GetAllBuses().Select<Bus, BusVO>(b =>
            {
                return new BusVO()
                {
                    Id = b.Id,
                     Name = b.Name,
                     
                };
            });
            logger.LogInformation("AddEvent ended");

            return View();
        }

        [HttpPost]
        public IActionResult AddEvent(AddEventVO addEventVO)
        {
            logger.LogInformation("AddEvent started", addEventVO);

            if (ModelState.IsValid)
            {              
                var events = eventRepository.GetAllEvents();
                bool isEventPresent = false;
                foreach (var item in events)
                {                   
                    if (item.Date.CompareTo(addEventVO.Date)==0 && item.BusId == addEventVO.BusId)
                    {
                        isEventPresent = true;
                        ModelState.AddModelError("", "Event is already schedule for bus with given date");
                        break;
                    }
                }

                if (!isEventPresent) {
                    Event _event = new Event()
                    {
                        StartPoint = addEventVO.StartPoint,
                        EndPoint = addEventVO.EndPoint,
                        Date = addEventVO.Date,
                        BusId = addEventVO.BusId
                    };

                    eventRepository.AddEvent(_event);

                    logger.LogInformation("AddEvent ended");
                    return RedirectToAction("Index");
                }
            }

            ViewBag.BusVO = busRepository.GetAllBuses().Select<Bus, BusVO>(b =>
            {
                logger.LogInformation("AddEvent ended");
                return new BusVO()
                {
                    Id = b.Id,
                    Name = b.Name,

                };
            });


            logger.LogInformation("AddEvent ended");

            return View(addEventVO);
        }

    }
}