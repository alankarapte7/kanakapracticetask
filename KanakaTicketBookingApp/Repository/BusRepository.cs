﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KanakaTicketBookingApp.Domain;
using KanakaTicketBookingApp.Models;


using Microsoft.EntityFrameworkCore;

namespace KanakaTicketBookingApp.Repository
{
    public class BusRepository : IBusRepository
    {
        private readonly AppDbContext appDbContext;

        public BusRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public Bus AddBus(Bus bus)
        {
            appDbContext.Bus.Add(bus);
            appDbContext.SaveChanges();
            return bus;
        }

        public IEnumerable<Bus> GetAllBuses()
        {
            return appDbContext.Bus;
        }

        public Bus GetById(long id)
        {

            return appDbContext.Bus.Find(id);
        }
    }
}
