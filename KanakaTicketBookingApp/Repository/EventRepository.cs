﻿using KanakaTicketBookingApp.Domain;
using KanakaTicketBookingApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Repository
{
    public class EventRepository : IEventRepository
    {
        private readonly AppDbContext appDbContext;

        public EventRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public Event AddEvent(Event _event)
        {
            appDbContext.Event.Add(_event);
            appDbContext.SaveChanges();
            return _event;
        }

        public IEnumerable<Event> GetAllEvents()
        {
            return appDbContext.Event.Include<Event, Bus>(e=> e.Bus);
        }
    }
}
