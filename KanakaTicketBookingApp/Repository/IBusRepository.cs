﻿using KanakaTicketBookingApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Repository
{
    public interface IBusRepository
    {
        Bus AddBus(Bus bus);
        Bus GetById(long id);
        IEnumerable<Bus> GetAllBuses();
    }
}
