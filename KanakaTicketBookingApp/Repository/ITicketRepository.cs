﻿using KanakaTicketBookingApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Repository
{
    public interface ITicketRepository
    {
        Ticket AddTicket(Ticket ticket);

        IEnumerable<Ticket> GetAllTickets();
        IEnumerable<Ticket> GetTicketByEvent(long eventId);
    }
}
