﻿using KanakaTicketBookingApp.Domain;
using KanakaTicketBookingApp.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Repository
{
    public class TicketRepository : ITicketRepository
    {
        private readonly AppDbContext appDbContext;

        public TicketRepository(AppDbContext appDbContext)
        {
            this.appDbContext = appDbContext;
        }

        public Ticket AddTicket(Ticket ticket)
        {
            appDbContext.Ticket.Add(ticket);
            appDbContext.SaveChanges();
            return ticket;
        }

        public IEnumerable<Ticket> GetAllTickets()
        {
            return appDbContext.Ticket.Include<Ticket, Event >(t => t.Event);
        }

        public IEnumerable<Ticket> GetTicketByEvent(long eventId)
        {
            return appDbContext.Ticket.Where<Ticket>(t =>
                t.EventId == eventId
            );
        }


    }
}
