﻿using KanakaTicketBookingApp.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KanakaTicketBookingApp.Repository
{
    public interface IEventRepository
    {
        Event AddEvent(Event _event);

        IEnumerable<Event> GetAllEvents();
    }
}
